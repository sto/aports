# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-calendar-tools
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by akonadi-calendar -> kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="CLI tools to manage akonadi calendars"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-calendar-dev
	akonadi-dev>=$pkgver
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcalutils-dev
	kdoctools-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/akonadi-calendar-tools.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1a5cd396cf9ddc845f9a799562b0a57514ae8bcaf526edafc75f96bf9098193a877c660c06dd53f0028be42e63211e55f19c499631579d326976867c8f33128d  akonadi-calendar-tools-23.08.0.tar.xz
"
