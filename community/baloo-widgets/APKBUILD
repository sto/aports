# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=baloo-widgets
pkgver=23.08.0
pkgrel=0
pkgdesc="Widgets for Baloo"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Baloo"
license="LGPL-2.0-only AND LGPL-2.1-or-later"
depends_dev="
	baloo-dev
	kconfig-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/libraries/baloo-widgets.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/baloo-widgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# filemetadatadatedisplaytest, filemetadatawidgettest and filemetadataitemcounttest
	# require running dbus server
	local skipped_tests="("
	local tests="
		filemetadatadatedisplay
		filemetadatawidget
		filemetadataitemcount"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)|test"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4f7f2d94489984cd8acc079d4fbfd392458f7b40f09b625d154be79396aec7cc634e4a3fde6140322fe4dbe5c27821edb79912bd6b03e89eea60bb045140f76d  baloo-widgets-23.08.0.tar.xz
"
