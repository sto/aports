# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=blinken
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/blinken/"
pkgdesc="Memory Enhancement Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/education/blinken.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/blinken-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d8078ca9a7551e3b61f42f6c9f1fa2a27b19363e36c2ad1740266f5a95990d1b8608bdc3f2b2f3ecc543711aea03f8abcca22c38a345d648b58bbd6b154ee398  blinken-23.08.0.tar.xz
"
