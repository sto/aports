# Contributor: André Klitzing <aklitzing@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=geth
pkgver=1.13.0
pkgrel=0
pkgdesc="Official Go implementation of the Ethereum protocol"
url="https://geth.ethereum.org/"
arch="all"
license="LGPL-3.0-or-later"
makedepends="go linux-headers"
checkdepends="fuse"
options="!check chmod-clean net"
source="$pkgname-$pkgver.tar.gz::https://github.com/ethereum/go-ethereum/archive/v$pkgver.tar.gz"
builddir="$srcdir/go-ethereum-$pkgver"

# secfixes:
#   1.10.22-r0:
#     - CVE-2022-37450

export GOROOT=/usr/lib/go
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make
}

check() {
	make test
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	install -m755 -t "$pkgdir"/usr/bin build/bin/*
}

sha512sums="
0b1f3c06d98b11a4f271fbdcb083a07a181601cd1ddbc2c918089c2df91b3e6e9acb433593d8fd508e873fcd23ecbcf0d8e436d862f84f2be6bf835f95908929  geth-1.13.0.tar.gz
"
