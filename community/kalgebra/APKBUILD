# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kalgebra
pkgver=23.08.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://edu.kde.org/kalgebra/"
pkgdesc="2D and 3D Graph Calculator"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	analitza-dev
	extra-cmake-modules
	kconfigwidgets-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	ncurses-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	readline-dev
	samurai
	"
_repo_url="https://invent.kde.org/education/kalgebra.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalgebra-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6c703a4fd4d83a21eda96cef82abb7675f27979ec99be192b8d434a10b098a8ae6898bddbb0d80189443279e02da934ec68fe52822c7ede459af9010c2ef27e1  kalgebra-23.08.0.tar.xz
"
