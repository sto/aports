# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdenlive
pkgver=23.08.0
pkgrel=0
# ppc64le mlt uses 64bit long double while libgcc uses 128bit long double
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> purpose
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kdenlive.org"
pkgdesc="An intuitive and powerful multi-track video editor, including most recent video technologies"
license="GPL-2.0-or-later"
depends="
	ffmpeg
	frei0r-plugins
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	knewstuff-dev
	knotifyconfig-dev
	kxmlgui-dev
	mlt-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtnetworkauth-dev
	qt5-qtsvg-dev
	rttr-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/multimedia/kdenlive.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7ef8bd4998ad98e98ab6d72db12107730e4e1b41ea64214d515dfe6f31bfc5a11921eacf4ede51cf6bc38f058e44d09b7f74f796daa41164b546d3c37d3ea7c5  kdenlive-23.08.0.tar.xz
"
