# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=killbots
pkgver=23.08.0
pkgrel=0
pkgdesc="A simple game of evading killer robots"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/killbots/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/killbots.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/killbots-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ff72a8208c46393a30b4ad7a23cd314c3267b0dad7eaa3214fd146a6edf0de6cd66a2a97be5f26c367e7beefb7c0b740d58dd2c5b6eaa7c02746c1bd04eb9a66  killbots-23.08.0.tar.xz
"
