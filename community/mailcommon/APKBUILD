# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=mailcommon
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# riscv64 disabled due to missing rust in recursive dependency
arch="all !ppc64le !s390x !armhf !riscv64" # Limited by messagelib -> qt5-qtwebengine
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
# TODO: Consider replacing gnupg with specific gnupg subpackages that mailcommon really needs.
depends="gnupg"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kitemviews-dev
	kmailtransport-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	mailimporter-dev
	messagelib-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	syntax-highlighting-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/mailcommon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7af6018308b5200938c0a562467ad3c754017e4f82baae580e8d7d1082fc5fe7f45d939a5abe5fb32174fc86bf341bf8bcfc0c774227553f0083780cd3632213  mailcommon-23.08.0.tar.xz
"
