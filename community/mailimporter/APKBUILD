# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=mailimporter
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE PIM library providing support for mail applications"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kmime-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/mailimporter.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/mailimporter-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0ea81b5a62ed5d570e4408212f8170fea9d1b5cfe95647ca87ea92c679c71fc139982a360be1d09737b2c0d86f6ccec6b44199b5e4e1f5fd9fc53f52f962a669  mailimporter-23.08.0.tar.xz
"
