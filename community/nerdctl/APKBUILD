# Maintainer: Jakub Panek <me@panekj.dev>
pkgname=nerdctl
pkgver=1.5.0
pkgrel=1
pkgdesc="Docker-compatible CLI for containerd"
url="https://github.com/containerd/nerdctl/"
arch="all"
license="Apache-2.0"
depends="ca-certificates containerd cni-plugins iptables"
makedepends="go"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/containerd/nerdctl/archive/refs/tags/v$pkgver.tar.gz"
options="!check" # a lot fail

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -ldflags "-X github.com/containerd/nerdctl/pkg/version.Version=$pkgver" \
		-o nerdctl ./cmd/nerdctl

	for shell in bash fish zsh; do
		./nerdctl completion $shell > nerdctl.$shell
	done
}

package() {
	install -Dm755 nerdctl -t "$pkgdir"/usr/bin
	install -Dm644 docs/*.md -t "$pkgdir"/usr/share/doc/$pkgname

	install -Dm644 nerdctl.bash \
		"$pkgdir"/usr/share/bash-completion/completions/nerdctl
	install -Dm644 nerdctl.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/nerdctl.fish
	install -Dm644 nerdctl.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_nerdctl
}

sha512sums="
d00b38147f32face001dba22ef71ff7751bdca2d249c9f9425b9513ac6b6cf2c8571b42e8d48ce02ae09e395386ecb4a84ecffdc95cea5353aa2220a81094d23  nerdctl-1.5.0.tar.gz
"
