# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-hcloud
pkgver=1.28.0
pkgrel=0
pkgdesc="Official Hetzner Cloud Python library"
url="https://github.com/hetznercloud/hcloud-python"
license="MIT"
arch="noarch"
depends="py3-dateutil py3-requests"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/hetznercloud/hcloud-python/archive/v$pkgver/py3-hcloud-$pkgver.tar.gz"
builddir="$srcdir/hcloud-python-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/hcloud-$pkgver-py3-none-any.whl
}

sha512sums="
fe720cf5b943f9535143f9389dc30e92ef295b7e5769b91e41ef979a5f6bbd059b42551818e7900e37213b530084eda51a54bad84a7ceb6e29f7f03d19bd4bbb  py3-hcloud-1.28.0.tar.gz
"
