# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-pydyf
_pyname=pydyf
pkgver=0.7.0
pkgrel=0
pkgdesc="A low-level PDF generator"
url="https://pypi.org/project/pydyf/"
arch="noarch"
license="BSD-3-Clause"
depends="python3"
makedepends="py3-flit-core py3-gpep517"
checkdepends="
	ghostscript
	py3-coverage
	py3-pillow
	py3-pytest
	py3-pytest-cov
	py3-pytest-flake8
	py3-pytest-isort
	py3-pytest-xdist
	"
_pypiprefix="${_pyname%"${_pyname#?}"}"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

prepare() {
	default_prepare

	# If you're going to make your test suite fail on flake8 errors you should
	# at least run it before you ship a release
	sed -i pyproject.toml -e 's/ --flake8//'
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/pydyf-$pkgver-py3-none-any.whl
}

sha512sums="
cfc58170b57684cbf5233ca48798881a980af0b3e3fdc8da91c5932affee0365f1e9d535df1f983e0f16c6e612ec5048c91d236bf69b6fae46c7e59f6c2388d0  pydyf-0.7.0.tar.gz
"
