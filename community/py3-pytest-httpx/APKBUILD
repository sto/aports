# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-pytest-httpx
pkgver=0.25.0
pkgrel=0
pkgdesc="send responses to httpx"
url="https://colin-b.github.io/pytest_httpx/"
arch="all !armhf !ppc64le" #limited by py3-httpx
license="MIT"
depends="py3-httpx py3-pytest"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Colin-b/pytest_httpx/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pytest_httpx-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	#deselected tests fail on [b''] not equal to []
	.testenv/bin/python3 -m pytest \
		--deselect tests/test_httpx_async.py::test_default_response_streaming \
		--deselect tests/test_httpx_sync.py::test_default_response_streaming
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
0e9be63d6a1c2e26bd26eae36e744adaa08d7189f04e2b342c1a6f13cfb4e3937af30720114723905b83c31720fa50b1137f95a1af20f7a8735d417961ec1bb2  py3-pytest-httpx-0.25.0.tar.gz
"
