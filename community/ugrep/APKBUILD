# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Francesco Camuffo <dev@fmac.xyz>
pkgname=ugrep
pkgver=4.0.5
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://github.com/Genivia/ugrep/wiki"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="bzip2-dev lz4-dev pcre2-dev xz-dev zlib-dev zstd-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
132b885a8abe141350122c24ab867433188bf68362ce4ec3f9823ffbbb50092ca6b4bbcf1688be978692480bc034c6e23cfbca77503c8b8b8063ef10be1ac803  ugrep-4.0.5.tar.gz
"
