# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=imlib2
pkgver=1.12.0
pkgrel=0
pkgdesc="Image manipulation library"
url="https://sourceforge.net/projects/enlightenment/"
arch="all"
license="Imlib2"
subpackages="$pkgname-dev"
depends_dev="freetype-dev libxext-dev libsm-dev"
makedepends="
	$depends_dev
	tiff-dev
	bzip2-dev
	giflib-dev
	libid3tag-dev
	libjpeg-turbo-dev
	libpng-dev
	libwebp-dev
	zlib-dev
	"
source="https://downloads.sourceforge.net/enlightenment/imlib2-$pkgver.tar.gz"

build() {
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc/imlib2 \
		--x-libraries=/usr/lib \
		--disable-static \
		--enable-visibility-hiding \
		--with-x \
		--with-bzip2 \
		--with-gif \
		--with-id3 \
		--with-jpeg \
		--with-png \
		--with-tiff \
		--with-webp \
		--with-zlib
	make
}

check() {
	make check
}

package() {
	make DESTDIR=$pkgdir install
}

sha512sums="
5a2f734ac11db199e1c90b92ff95374dc14f053b148dc56f3c19b8b599d711bd853391affa5533e9dcbf465907740190e7fd3bd04db30c18c013f60d240c6a64  imlib2-1.12.0.tar.gz
"
