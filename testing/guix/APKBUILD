# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=guix
pkgver=1.4.0
pkgrel=1
pkgdesc="Functional package manager based on Guile Scheme"
url="https://guix.gnu.org"
# s390x not supported upstream
arch="all !s390x"
license="GPL-3.0-or-later"
depends="
	guile
	guile-gcrypt
	guile-git
	guile-gnutls
	guile-json
	guile-lzlib
	guile-sqlite3
	guile-zlib
	guile-zstd"
makedepends="
	argp-standalone
	bzip2-dev
	gettext-dev
	guile-dev
	libgcrypt-dev
	po4a
	sqlite-dev
	texinfo"
install="guix.pre-install"
subpackages="
	$pkgname-doc
	$pkgname-lang
	$pkgname-openrc
	$pkgname-zsh-completion
	$pkgname-fish-completion
	$pkgname-bash-completion"
options="!strip" # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907061
source="https://ftp.gnu.org/gnu/guix/guix-$pkgver.tar.gz
	improve-openrc-service.patch
	guix.sh

	0001-syscalls-Consistently-use-existing-linux-definition.patch
	0002-syscalls-Recognize-musl-linux-as-a-linux-target.patch
	0003-syscalls-Use-readdir-instead-of-readdir64-on-musl.patch
	0004-daemon-Fix-build-with-GCC13.patch
	0005-syscalls-add-workaround-for-lack-of-stat64-symbol-on.patch"

build() {
	local guix_system="$CARCH-linux"
	case $CARCH in
	arm*)    guix_system="armhf-linux"       ;;
	x86)     guix_system="i686-linux"        ;;
	ppc64le) guix_system="powerpc64le-linux" ;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-system="$guix_system" \
		--with-bash-completion-dir=/usr/share/bash-completion/completions \
		--with-fish-completion-dir=/usr/share/fish/vendor_completions.d \
		--disable-rpath
	make LIBS="-lintl -largp"
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/guix.sh \
		"$pkgdir"/etc/profile.d/guix.sh

	# Fixup the guix-daemon OpenRC service.
	rm -r "$pkgdir"/etc/init.d
	mv "$pkgdir"/etc/openrc "$pkgdir"/etc/init.d
	chmod +x "$pkgdir"/etc/init.d/guix-daemon

	# Remove other files that we don't use.
	rm -r "$pkgdir"/usr/lib/upstart \
		"$pkgdir"/usr/lib/systemd \
		"$pkgdir"/usr/share/selinux

	# Add /etc/guix/acl with the default substitute servers.
	# Taken from: https://salsa.debian.org/debian/guix/-/blob/2d44a707ccaacbee73410b770aa1a395eff1caa6/debian/rules#L57
	mkdir -p "$pkgdir"/etc/guix
	{
		printf '(acl\n (entry\n'
		sed -e 's,^,  ,g' -e 's, $$,,g' etc/substitutes/ci.guix.gnu.org.pub
		printf '  (tag\n   (guix import)\n   )\n  )\n (entry\n'
		sed -e 's,^,  ,g' -e 's, $$,,g' etc/substitutes/bordeaux.guix.gnu.org.pub
		printf '  (tag\n   (guix import)\n   )\n  )\n )\n'
	} > "$pkgdir"/etc/guix/acl
}

sha512sums="
b15a66d89fa9a6cae1a2f4bc8260876b82657e39cd1f961c50580d7ab05c6024107161d8cda2378c8e258826f85a24da1f95e76ce3818ec87a73cbc52214e430  guix-1.4.0.tar.gz
6fd700c087ac70911d8472cdf3baf6abe8f0a581ff5c19da1d2519c6320f2c8214d17356c28f206d80ab39cf4c9600c0d49901b69da8433b04d3e4d7f3dda8eb  improve-openrc-service.patch
2f4058819ac6c744f137cfae7ff036b17ac427aafed749060488ae056e1eeb4a0e1cdeddf7af40bc59b21dbcb9932434071edb8ff78da0089df83f2ded22aefe  guix.sh
874c4d32338cb99894c958a7be4ed1bfb0bde162efce5fa8439c6482ea51113b6a796f59d4ef8dbbe43bbdba687e8c3f8d6f30790d6a808f2b0c7ac0d4265585  0001-syscalls-Consistently-use-existing-linux-definition.patch
c04c3073b9434e173f203ad51cc8c44aadb771681eff5359519a2397663db6783478faeebe6bb29facf24039610915f85165c2f839ad47e9d6de00a80a4d9304  0002-syscalls-Recognize-musl-linux-as-a-linux-target.patch
9f7e12c752f336391363dce2d855416b15478a82a8d844422f94200f4a7a60badd175d46f7acde569fa25fa5cae2aeca88282f9e3f4471d91078eb013bd0e099  0003-syscalls-Use-readdir-instead-of-readdir64-on-musl.patch
17f4ee6189d753b8d818d7be1086dfc0bb68751ef796b4eff4c49766ab3a78272275cf5710d9cec20aca299c9e2091607a77e104145ed20abefa3c46069933c8  0004-daemon-Fix-build-with-GCC13.patch
b05735725e3136a355abf363f16dc37d8c908c10a8794e747cc9e49b85ce1b645fbeb54dfc75b87c90950da605d821f2bf87c1d5b50ad40657645bbf7686e608  0005-syscalls-add-workaround-for-lack-of-stat64-symbol-on.patch
"
