# Contributor: Lauren N. Liberda <lauren@selfisekai.rocks>
# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=py3-pathvalidate
pkgver=3.1.0
pkgrel=0
pkgdesc="Python library to sanitize/validate a string such as filenames/file-paths/etc"
url="https://github.com/thombashi/pathvalidate"
arch="noarch"
license="MIT"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-click py3-faker"
options="!check" # tests require unpackaged unmaintained dependencies
subpackages="$pkgname-pyc"
source="https://github.com/thombashi/pathvalidate/archive/refs/tags/v$pkgver/pathvalidate-$pkgver.tar.gz"
builddir="$srcdir/pathvalidate-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
8f1d9905c5183ab66f20b9bbd69db0f0547ffde07333a36425f66b76191de47236bf1e0809b0cf4d181ea6714aeaef03e1b05dc1d6599e0d359cb96cdbec5db4  pathvalidate-3.1.0.tar.gz
"
