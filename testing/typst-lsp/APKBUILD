# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=typst-lsp
pkgver=0.9.5
pkgrel=0
pkgdesc="Language server for typst"
url="https://github.com/nvarner/typst-lsp"
# typst, rust-analyzer
arch="aarch64 ppc64le x86_64"
license="MIT"
depends="rust-analyzer"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
source="
	https://github.com/nvarner/typst-lsp/archive/refs/tags/v$pkgver/typst-lsp-$pkgver.tar.gz
	"
options="net !check" # no tests

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build \
		--release \
		--frozen \
		--no-default-features \
		--features=remote-packages,native-tls
}

package() {
	install -Dm755 target/release/typst-lsp -t "$pkgdir"/usr/bin/
}

sha512sums="
d0b07cf7f055f4854762a2293a15921c87d9637cf28fc99b8092a2219f90f904b33f7cbe3f012085488ca04835e09ce1fc949a63460602099b7caa5c64511d12  typst-lsp-0.9.5.tar.gz
"
