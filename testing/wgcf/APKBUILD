# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=wgcf
pkgver=2.2.18
pkgrel=1
pkgdesc="Unofficial CLI for Cloudflare Warp"
url="https://github.com/ViRb3/wgcf"
arch="all"
license="MIT"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/ViRb3/wgcf/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v

	for shell in bash fish zsh; do
		./wgcf completion $shell > wgcf.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 wgcf -t "$pkgdir"/usr/bin/

	install -Dm644 wgcf.bash \
		"$pkgdir"/usr/share/bash-completion/completions/wgcf
	install -Dm644 wgcf.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/wgcf.fish
	install -Dm644 wgcf.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_wgcf
}

sha512sums="
81c252f580317c011365dd88143ef500cc1fb528d209c8a5999c9b8e0ab9d05e47851657123073d8d986246fc8496f501deda02af8a4d9073b954bfeb311c9c7  wgcf-2.2.18.tar.gz
"
